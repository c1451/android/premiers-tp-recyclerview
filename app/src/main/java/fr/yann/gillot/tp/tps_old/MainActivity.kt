package fr.yann.gillot.tp.tps_old

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import fr.yann.gillot.tp.R
import fr.yann.gillot.tp.randomAvatar.view.RandomAvatarActivity

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun onClickButtonLaunchTp1RecyclerView(view: android.view.View) {
        val intent = Intent(this, PersonRecyclerViewActivity::class.java)
        startActivity(intent)
    }

    fun onClickButtonLaunchApiRecyclerView(view: android.view.View) {
        val intent = Intent(this, RandomAvatarActivity::class.java)
        startActivity(intent)
    }
}