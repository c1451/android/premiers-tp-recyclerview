package fr.yann.gillot.tp.randomAvatar.dao

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import fr.yann.gillot.tp.randomAvatar.model.RandomAvatarRoom

@Dao
interface RandomAvatarDao {


    @Query("SELECT * FROM random_avatar")
    fun selectAll() : LiveData<List<RandomAvatarRoom>>


    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(randomAvatarRoom: RandomAvatarRoom)


    @Query("DELETE FROM random_avatar")
    fun deleteAll()
}
