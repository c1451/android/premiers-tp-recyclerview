package fr.yann.gillot.tp.randomAvatar.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.ViewModel
import fr.yann.gillot.tp.randomAvatar.model.RandomAvatarRoom
import fr.yann.gillot.tp.randomAvatar.model.RandomAvatarUi
import fr.yann.gillot.tp.randomAvatar.repository.RandomAvatarRepository
import kotlinx.coroutines.Dispatchers
import androidx.lifecycle.map
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.launch


class RandomAvatarViewModel : ViewModel() {
    private val mRandomAvatarRepository: RandomAvatarRepository by lazy { RandomAvatarRepository() }
    var mRandomAvatarLiveData: LiveData<List<RandomAvatarUi>> =
        mRandomAvatarRepository.selectAllRandomAvatar().map {
            it.toUi()
        }


    fun fetchNewAvatar() {
        viewModelScope.launch(Dispatchers.IO) {
            mRandomAvatarRepository.fetchData()
        }
    }


    fun deleteAll() {
        viewModelScope.launch(Dispatchers.IO) {
            mRandomAvatarRepository.deleteAllRandomAvatar()
        }
    }
}


private fun List<RandomAvatarRoom>.toUi(): List<RandomAvatarUi> {
    return asSequence().map {
        RandomAvatarUi(
            firstName = it.firstName,
            lastName = it.lastName,
            avatarUrl = it.avatarUrl
        )
    }.toList()
}
