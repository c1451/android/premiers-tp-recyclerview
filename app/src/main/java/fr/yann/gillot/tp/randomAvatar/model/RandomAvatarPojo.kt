package fr.yann.gillot.tp.randomAvatar.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

/** Object use for room */
@Entity(tableName = "random_avatar")
data class RandomAvatarRoom(
    @ColumnInfo(name = "first_name")
    val firstName: String,

    @ColumnInfo(name = "last_name")
    val lastName: String,

    @ColumnInfo(name = "avatar")
    val avatarUrl: String
) {
    @PrimaryKey(autoGenerate = true)
    var id: Long = 0
}

/** Object use for Ui */
data class RandomAvatarUi(
    val firstName: String,
    val lastName: String,
    val avatarUrl: String
)

/** Object use for retrofit */
data class RandomAvatarRetrofit(
    @Expose
    @SerializedName("first_name")
    val firstName: String,

    @Expose
    @SerializedName("last_name")
    val lastName: String,

    @Expose
    @SerializedName("avatar")
    val avatarUrl: String
)


