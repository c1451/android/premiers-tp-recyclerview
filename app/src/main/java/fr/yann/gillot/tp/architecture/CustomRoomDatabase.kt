package fr.yann.gillot.tp.architecture

import androidx.room.Database
import androidx.room.RoomDatabase
import fr.yann.gillot.tp.randomAvatar.dao.RandomAvatarDao
import fr.yann.gillot.tp.randomAvatar.model.RandomAvatarRoom

@Database(
    entities = [
        RandomAvatarRoom::class
    ],
    version = 7,
    exportSchema = false
)
abstract class CustomRoomDatabase : RoomDatabase() {
    abstract fun randomAvatarDao() : RandomAvatarDao
}
