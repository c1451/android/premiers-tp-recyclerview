package fr.yann.gillot.tp.TPS.model

sealed class PersonForRecyclerView()

data class PersonDataSample(
    val firstname : String,
    val lastname : String,
    val height : Int, //centimeter
    val weight : Float, //kg
    val sexe : Char
): PersonForRecyclerView()

data class PersonHeaderSample(
    val header: String
): PersonForRecyclerView()

data class PersonFooterSample(
    val header: String
): PersonForRecyclerView()

