package fr.yann.gillot.tp.randomAvatar.repository

import androidx.lifecycle.LiveData
import fr.yann.gillot.tp.architecture.CustomApplication
import fr.yann.gillot.tp.architecture.RetrofitBuilder
import fr.yann.gillot.tp.randomAvatar.model.RandomAvatarRetrofit
import fr.yann.gillot.tp.randomAvatar.model.RandomAvatarRoom
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class RandomAvatarRepository {
    private val mRandomAvatarDao = CustomApplication.instance.mApplicationDatabase.randomAvatarDao()

    fun selectAllRandomAvatar(): LiveData<List<RandomAvatarRoom>> {
        return mRandomAvatarDao.selectAll()
    }


    private suspend fun insertRandomAvatar(randomAvatar: RandomAvatarRoom) =
        withContext(Dispatchers.IO) {
            mRandomAvatarDao.insert(randomAvatar)
        }


    suspend fun deleteAllRandomAvatar() = withContext(Dispatchers.IO) {
        mRandomAvatarDao.deleteAll()
    }


    suspend fun fetchData() {
        insertRandomAvatar(RetrofitBuilder.getRandomAvatar().getRandomAvatar().toRoom())
    }
}


private fun RandomAvatarRetrofit.toRoom(): RandomAvatarRoom {
    return RandomAvatarRoom(
        firstName = firstName,
        lastName = lastName,
        avatarUrl = avatarUrl
    )
}