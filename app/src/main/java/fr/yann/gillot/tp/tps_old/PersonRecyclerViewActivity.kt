package fr.yann.gillot.tp.tps_old

import android.os.Bundle
import android.view.HapticFeedbackConstants
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.yann.gillot.tp.TPS.model.PersonDataSample
import fr.yann.gillot.tp.TPS.model.PersonFooterSample
import fr.yann.gillot.tp.TPS.model.PersonForRecyclerView
import fr.yann.gillot.tp.TPS.model.PersonHeaderSample
import fr.yann.gillot.tp.databinding.ActivityPersonRecyclerViewBinding

class PersonRecyclerViewActivity : AppCompatActivity() {
    private lateinit var binding : ActivityPersonRecyclerViewBinding
    private lateinit var personAdapter: PersonAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityPersonRecyclerViewBinding.inflate(layoutInflater)
        setContentView(binding.root)

        personAdapter = PersonAdapter { item, view ->
            onItemClick(item, view)
        }

        binding.recyclerView.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)

        binding.recyclerView.adapter = personAdapter
        personAdapter.submitList(generateFakeData())
    }

    private fun generateFakeData(): MutableList<PersonForRecyclerView> {
        val result = mutableListOf<PersonForRecyclerView>()
        mutableListOf(
            PersonDataSample("Anthony", "Kit", 182, 56.2.toFloat(),'M'),
            PersonDataSample("Yann", "Gillot", 180, 57.3.toFloat(),'M'),
            PersonDataSample("Terrence", "Ardenois", 190, 51.toFloat(),'F'),
        ).groupBy {
            // liste selon le sexe
            it.sexe
        }.forEach { (sexe, items) ->
            result.add(PersonHeaderSample("Sexe : $sexe"))
            result.addAll(items)
            result.add(PersonFooterSample("$sexe totaux : " + items.size.toString()))
        }
        return result
    }

    private fun onItemClick(person: PersonDataSample, view : View) {
        view.performHapticFeedback(HapticFeedbackConstants.VIRTUAL_KEY)
        Toast.makeText(this, person.lastname + " " + person.firstname, Toast.LENGTH_LONG).show()
    }

}