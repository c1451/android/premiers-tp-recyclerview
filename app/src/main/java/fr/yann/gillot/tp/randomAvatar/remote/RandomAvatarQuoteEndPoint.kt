package fr.yann.gillot.tp.randomAvatar.remote

import fr.yann.gillot.tp.randomAvatar.model.RandomAvatarRetrofit
import retrofit2.http.GET

interface RandomAvatarEndpoint {


    @GET("users/random_user")
    suspend fun getRandomAvatar() : RandomAvatarRetrofit
}
