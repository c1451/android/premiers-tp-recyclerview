package fr.yann.gillot.tp.randomAvatar.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import fr.yann.gillot.tp.databinding.ActivityRandomAvatarBinding
import fr.yann.gillot.tp.randomAvatar.model.RandomAvatarUi
import fr.yann.gillot.tp.randomAvatar.viewmodel.RandomAvatarViewModel

class RandomAvatarActivity : AppCompatActivity() {


    private lateinit var viewModel: RandomAvatarViewModel
    private lateinit var binding : ActivityRandomAvatarBinding
    private val adapter : RandomAvatarAdapter = RandomAvatarAdapter()
    private val observer = Observer<List<RandomAvatarUi>> {
        adapter.submitList(it)
    }
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityRandomAvatarBinding.inflate(layoutInflater)
        setContentView(binding.root)


        viewModel = ViewModelProvider(this)[RandomAvatarViewModel::class.java]


        binding.randomAvatarActivityRv.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        binding.randomAvatarActivityRv.adapter = adapter


        binding.randomAvatarActivityAdd.setOnClickListener {
            viewModel.fetchNewAvatar()
        }


        binding.randomAvatarActivityDelete.setOnClickListener {
            viewModel.deleteAll()
        }
    }
    override fun onStart() {
        super.onStart()
        viewModel.mRandomAvatarLiveData.observe(this, observer)
    }


    override fun onStop() {
        viewModel.mRandomAvatarLiveData.removeObserver(observer)
        super.onStop()
    }
}