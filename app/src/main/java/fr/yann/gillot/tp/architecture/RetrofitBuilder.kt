package fr.yann.gillot.tp.architecture

import com.google.gson.GsonBuilder
import fr.yann.gillot.tp.randomAvatar.remote.RandomAvatarEndpoint
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

object RetrofitBuilder {
    private val retrofit: Retrofit = Retrofit.Builder()
        .baseUrl("https://random-data-api.com/api/")
        .addConverterFactory(GsonConverterFactory.create(GsonBuilder().excludeFieldsWithoutExposeAnnotation().create()))
        .build()




    fun getRandomAvatar(): RandomAvatarEndpoint = retrofit.create(RandomAvatarEndpoint::class.java)
}
