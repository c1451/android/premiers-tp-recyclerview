package fr.yann.gillot.tp.randomAvatar.view

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import fr.yann.gillot.tp.databinding.ItemRandomAvatarBinding
import fr.yann.gillot.tp.randomAvatar.model.RandomAvatarUi

val diffUtils = object : DiffUtil.ItemCallback<RandomAvatarUi>() {
    override fun areItemsTheSame(oldItem: RandomAvatarUi, newItem: RandomAvatarUi): Boolean {
        return oldItem == newItem
    }


    override fun areContentsTheSame(oldItem: RandomAvatarUi, newItem: RandomAvatarUi): Boolean {
        return oldItem == newItem
    }
}

class RandomAvatarViewHolder(
    val binding: ItemRandomAvatarBinding
) : RecyclerView.ViewHolder(binding.root) {


    private lateinit var ui: RandomAvatarUi


    fun bind(randomAvatarUi: RandomAvatarUi) {
        ui = randomAvatarUi
        Glide.with(itemView.context)
            .load(randomAvatarUi.avatarUrl)
            .into(binding.itemRandomAvatarIcon)


        binding.itemRandomAvatarFirstName.text = randomAvatarUi.firstName
        binding.itemRandomAvatarLastName.text = randomAvatarUi.lastName
    }
}

class RandomAvatarAdapter : ListAdapter<RandomAvatarUi, RandomAvatarViewHolder>(diffUtils) {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RandomAvatarViewHolder {
        return RandomAvatarViewHolder(
            ItemRandomAvatarBinding.inflate(
                LayoutInflater.from(parent.context),
                parent,
                false
            )
        )
    }


    override fun onBindViewHolder(holder: RandomAvatarViewHolder, position: Int) {
        holder.bind(getItem(position))
    }
}

