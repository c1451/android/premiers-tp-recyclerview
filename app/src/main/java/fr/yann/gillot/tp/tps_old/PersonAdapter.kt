package fr.yann.gillot.tp.tps_old

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import fr.yann.gillot.tp.TPS.model.PersonDataSample
import fr.yann.gillot.tp.TPS.model.PersonFooterSample
import fr.yann.gillot.tp.TPS.model.PersonForRecyclerView
import fr.yann.gillot.tp.TPS.model.PersonHeaderSample
import fr.yann.gillot.tp.databinding.PersonCustomRecyclerFooterBinding
import fr.yann.gillot.tp.databinding.PersonCustomRecyclerHeaderBinding
import fr.yann.gillot.tp.databinding.PersonItemRecyclerBinding

private val diffItemUtils = object : DiffUtil.ItemCallback<PersonForRecyclerView>() {


    override fun areItemsTheSame(
        oldItem: PersonForRecyclerView,
        newItem: PersonForRecyclerView
    ): Boolean {
        return oldItem == newItem
    }


    override fun areContentsTheSame(
        oldItem: PersonForRecyclerView,
        newItem: PersonForRecyclerView
    ): Boolean {
        return oldItem == newItem
    }
}

class PersonViewHolder(
    private val binding: PersonItemRecyclerBinding,
    onItemClick: (personDataSample: PersonDataSample, view: View) -> Unit
) : RecyclerView.ViewHolder(binding.root) {


    private lateinit var ui: PersonDataSample


    init {
        binding.root.setOnClickListener {
            onItemClick(ui, itemView)
        }
    }


    fun bind(personDataSample: PersonDataSample) {
        ui = personDataSample
        binding.itemRecyclerViewFirstName.text = personDataSample.firstname
        binding.itemRecyclerViewLastName.text = personDataSample.lastname
        binding.itemRecyclerViewHeight.text = "${personDataSample.height}" + " cm"
        binding.itemRecyclerViewWeight.text = "${personDataSample.weight}" + " kg"
    }
}

class PersonHeaderViewHolder(
    private val binding: PersonCustomRecyclerHeaderBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(personDataFooterSample: PersonHeaderSample) {
        binding.itemRecyclerViewHeader.text = personDataFooterSample.header
    }
}

class PersonFooterViewHolder(
    private val binding: PersonCustomRecyclerFooterBinding
) : RecyclerView.ViewHolder(binding.root) {
    fun bind(personDataFooterSample: PersonFooterSample) {
        binding.itemRecyclerViewHeader.text = personDataFooterSample.header
    }
}

class PersonAdapter(
    private val onItemClick: (avatarUi: PersonDataSample, view: View) -> Unit,
) :
    ListAdapter<PersonForRecyclerView, RecyclerView.ViewHolder>(diffItemUtils) {


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) =
        when (holder.itemViewType) {
            MyItemType.ROW.type -> (holder as PersonViewHolder).bind(getItem(position) as PersonDataSample)

            MyItemType.HEADER.type -> (holder as PersonHeaderViewHolder).bind(
                getItem(
                    position
                ) as PersonHeaderSample
            )

            MyItemType.FOOTER.type -> (holder as PersonFooterViewHolder).bind(
                getItem(
                    position
                ) as PersonFooterSample
            )


            else -> throw RuntimeException("Wrong view type received ${holder.itemView}")
        }


    override fun getItemViewType(position: Int): Int {
        return when (getItem(position)) {
            is PersonDataSample -> MyItemType.ROW.type
            is PersonHeaderSample -> MyItemType.HEADER.type
            is PersonFooterSample -> MyItemType.FOOTER.type
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        when (viewType) {
            MyItemType.ROW.type -> {
                PersonViewHolder(
                    PersonItemRecyclerBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    ), onItemClick
                )
            }


            MyItemType.HEADER.type -> {
                PersonHeaderViewHolder(
                    PersonCustomRecyclerHeaderBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }

            MyItemType.FOOTER.type -> {
                PersonFooterViewHolder(
                    PersonCustomRecyclerFooterBinding.inflate(
                        LayoutInflater.from(parent.context),
                        parent,
                        false
                    )
                )
            }

            else -> throw RuntimeException("Wrong view type received $viewType")
        }

}


enum class MyItemType(val type: Int) {
    ROW(0),
    HEADER(1),
    FOOTER(2)
}